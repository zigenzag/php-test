@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pokedex - Search</div>

                <div class="card-body">
                    <p>If you want to find a Pokemon by Name or ID then you can enter your search into the field below:</p>
                    <form class="form-inline my-2 my-lg-0" action="/pokemon/search" method="POST" role="search">
                        {{ csrf_field() }}
                        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <p class="mt-3 mb-0">Alternatively, check out our <a href="/pokemon/popular" title="popular Pokemon">popular Pokemon</a> or trawl through the <a href="/pokemon/list" title="full list">full list</a>!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
