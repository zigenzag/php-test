@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pokedex - {{ $title }}</div>

                <div class="card-body">
                    <ul class="list-unstyled">
                        @foreach ($pokemon as $mon)
                        <li class="list-group-item">
                            <a href="/pokemon/id/{{ $mon->id }}">
                                <div class="media">
                                    <img class="align-self-center mr-3" height="96" width="96" src="{{ $mon->sprite }}" alt=""{{ ucwords( $mon->name ) }}/>
                                    <div class="media-body">
                                        <h4 class="mt-0">{{ ucwords( str_replace('-', ' ', $mon->name ) ) }} - #{{ $mon->id }}</h4>
                                        <p class="mb-0">
                                            Height: {{ $mon->height }}
                                        </p>
                                        <p class="mb-0">
                                            Weight: {{ $mon->weight }} 
                                        </p> 
                                    </div>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @if (!empty($paginate))
                        <div class="mt-4">
                            {{ $pokemon->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
