@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pokedex - {{ ucwords( str_replace('-', ' ', $pokemon->name ) ) }}</div>
                <div class="card-body">
                    <div class="media">
                        <img class="mr-3" height="96" width="96" src="{{ $pokemon->sprite }}" alt=""{{ ucwords( $pokemon->name ) }}/>
                        <div class="media-body">
                            <h4 class="mt-0">{{ ucwords( str_replace('-', ' ', $pokemon->name ) ) }} - #{{ $pokemon->id }}</h4>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Height: {{ $pokemon->height }}</li>
                                <li class="list-group-item">Weight: {{ $pokemon->weight }}</li>
                                <li class="list-group-item">Species: {{ ucwords( str_replace('-', ' ', $pokemon->species ) ) }}</li>
                            </ul>
                            @if(!empty($abilities))
                            <h5 class="mt-3">Abilities</h5>
                            <ul class="list-group">
                                @foreach($abilities as $ability)
                                    <li class="list-group-item"><strong>{{ ucwords( str_replace('-', ' ', $ability->name ) ) }}</strong> - {{ $ability->effect_short }}</li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
