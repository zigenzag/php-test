@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome to Bradley's Pokedex</div>

                <div class="card-body">
                    <p>Hello and welcome to Bradley's Pokedex. If you want to look up a Pokemon, then you can either use the search feature, or view the options under the Pokemon dropdown menu. These features are avaliable in the navigation bar above.</p>
                    <p>To get started quickly, check out our <a href="/pokemon/popular" title="popular Pokemon">popular Pokemon</a> or trawl through the <a href="/pokemon/list" title="full list">full list</a>!</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
