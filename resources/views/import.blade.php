@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pokedex - Import Data</div>

                <div class="card-body">
                    <p class="mb-0">
                    @if(!empty($status))
                        Import in progress, do not leave this page! So far roughly <strong>{{ $offset }}</strong> Pokemon have been imported!
                    @else
                        Finished importing Pokemon! <!-- Could do with a count of how many were imported! -->
                    @endif
                    </p>
                </div>
                @if(!empty($status))
                <script>
                    setTimeout(function() {
                        window.location.replace("/import/{{ $offset }}");
                    }, 2500);
                </script>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
