<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PokemonApi;


class PokemonApiController extends Controller
{
    public function importList(Request $request){
        $pokemonApi = new PokemonApi();
        $offset = $request->route('offset');
        if( empty($offset)){
            $offset = 0;
        }
        $status = $pokemonApi->importPokemonList($offset);
        $offset += 15;
        return view('import',['offset' => $offset, 'status' => $status]);
    }

    public function index(){
        
    }
}
