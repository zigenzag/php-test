<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pokemon;
use App\Ability;

class PokemonController extends Controller
{
    public function single($id)
    {
        $pokemon = Pokemon::find($id);
        $abilites_array = json_decode($pokemon->abilities);
        $abilites = Ability::whereIn('id',$abilites_array)->get();
        return view('pokemon.single', ['pokemon' => $pokemon, 'abilities' => $abilites]);
    }

    public function list()
    {
        $title = 'List Pokemon';
        $pokemon = Pokemon::paginate(10);
        return view('pokemon.list', ['pokemon' => $pokemon, 'title' => $title, 'paginate' => true ]);
    }

    public function popular()
    {
        $title = 'Popular Pokemon';
        $pokemon_ids_array = [25,7,4,10,52,54,93,50,43,16];
        $pokemon = Pokemon::whereIn('id', $pokemon_ids_array)->orderBy('name', 'asc')->get();
        return view('pokemon.list', ['pokemon' => $pokemon, 'title' => $title, 'paginate' => false ]);
    }

    public function search(Request $request)
    {
        $term = preg_replace('/[^a-zA-Z0-9]/', '', $request->search);
        if(empty($term)) return view('pokemon.search');
        $title = 'Search: ' . $term;
        $pokemon = Pokemon::where('name', $term)
        ->orWhere('name', 'like', '%' . $term . '%')
        ->orWhere('id', $term)
        ->get();
        return view('pokemon.list', ['pokemon' => $pokemon, 'title' => $title, 'paginate' => false ]);
    }
}
