<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\Pokemon;
use App\Ability;

class PokemonApi extends Model
{
   
    public function getPokemon($id){
        $pokemon = Pokemon::find($id);
        /* If Pokemon doesn't exist... exit! Could do with a 404 if it was to be developed further */
        if(empty($pokemon)) return false;
        $this->importPokemon($id, $url);
        $pokemon = Pokemon::find($id);
        return !empty($pokemon) ? $pokemon : false;
    }

    private function getPokemonList($offset = 0){
        $url = 'https://pokeapi.co/api/v2/pokemon/?offset=' . $offset . '&limit=15';
        $contents = $this->callApi($url);
        return $contents;
    }

    public function importPokemonList($offset = 0){
        /* Get list of Pokemon to import. If there are no results then we have gotten to the end of the import */
        $results = $this->getPokemonList($offset)->results;
        if(empty( $results )){
            return false;
        }
        foreach($results as $item){
            $this->importPokemon($item->url);
        }
        return true;
    }

    private function importPokemon($url){
        $id = str_replace('/', '', str_replace('https://pokeapi.co/api/v2/pokemon/', '', $url));
        
        /* Checks to see if Pokemon exists in DB */
        $pokemon = Pokemon::where('id', '=', $id)->get();
        foreach( $pokemon as $mon ){
            if(!empty($mon) && (!empty($mon) && $mon->id == $id) ){
                return false;
            }
        }

        /* Call the API for the pokemon */
        $contents = $this->callApi($url);

        /* Get the ID of the abilities from the url's. Then put these through the importer. */
        $abilities = [];
        foreach( $contents->abilities as $ability){
            $abilities[] = $this->importAbility($ability->ability->url);
        }

        $species_content = $this->callApi($contents->species->url);

        /* Create Pokemon */
        $pokemon = new Pokemon();
        $pokemon->id        = $contents->id;
        $pokemon->name      = $contents->name;
        $pokemon->height    = $contents->height;
        $pokemon->weight    = $contents->weight;
        $pokemon->species   = $species_content->name;
        $pokemon->sprite    = $contents->sprites->front_default;
        $pokemon->abilities = json_encode( $abilities );
        $pokemon->save();

        return true;
    }

    private function importAbility($url){
        $id = str_replace('/', '', str_replace('https://pokeapi.co/api/v2/ability/', '', $url));

        /* Checks to see if ability exists in DB */
        $ability = Ability::find($id);
        if( !empty( $ability ) && $ability->id == $id){
            return $ability->id;
        }

        $contents = $this->callApi($url);

        $pokemon = [];
        foreach($contents->pokemon as $pokemon){
            $pokemon = $pokemon->pokemon->name;
        }

        /* Create Ability */
        $ability  = new Ability();
        $ability->id            = $contents->id;
        $ability->name          = $contents->name;
        $ability->effect        = $contents->effect_entries[0]->effect;
        $ability->effect_short  = $contents->effect_entries[0]->short_effect;
        $ability->pokemon       = json_encode($pokemon);
        $ability->url           = $url;
        $ability->save();

        return $contents->id;
    }

    private function callApi($url = null){
        if(empty($url)) return false;
        $client = new Client();
        $request = $client->get($url);
        $contents = json_decode($request->getBody()->getContents());
        return $contents;
    }
}
