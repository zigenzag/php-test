<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/pokemon', 'PokemonController@list');
Route::get('/pokemon/list', 'PokemonController@list');
Route::get('/pokemon/popular', 'PokemonController@popular');
Route::get('/pokemon/id/{id}', 'PokemonController@single')->where(['id' => '[0-9]+']);

Route::get('/pokemon/search', 'PokemonController@search');
Route::post('/pokemon/search', 'PokemonController@search');

Route::get('/import', 'PokemonApiController@importList');
Route::get('/import/{offset}', 'PokemonApiController@importList')->where(['offset' => '[0-9]+']);
